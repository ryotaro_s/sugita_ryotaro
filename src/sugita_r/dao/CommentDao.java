package sugita_r.dao;

import static sugita_r.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import sugita_r.beans.Comment;
import sugita_r.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comment ( ");
            sql.append("comment");
            sql.append(", registered_person");
            sql.append(", messageid");
            sql.append(") VALUES (");
            sql.append(" ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, comment.getComment());
            ps.setString(2, comment.getRegistered_person());;
            ps.setInt(3, comment.getMessageid());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public List<Comment> getComment(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT* ");
            sql.append("FROM comment ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Comment> ret = toCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Comment> toCommentList(ResultSet rs)
            throws SQLException {

        List<Comment> ret = new ArrayList<Comment>();
        try {
            while (rs.next()) {
                String comment = rs.getString("comment");
                Timestamp createdDate = rs.getTimestamp("created_date");
                String registeredPerson = rs.getString("registered_person");
                int id = rs.getInt("id");
                int messageid = rs.getInt("messageid");

                Comment usercomment = new Comment();
                usercomment.setComment(comment);
                usercomment.setCreatedDate(createdDate);
                usercomment.setRegistered_person(registeredPerson);
                usercomment.setId(id);
                usercomment.setMessageid(messageid);

                ret.add(usercomment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}