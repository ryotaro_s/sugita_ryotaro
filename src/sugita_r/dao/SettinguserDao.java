package sugita_r.dao;

import static sugita_r.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sugita_r.beans.User;
import sugita_r.exception.NoRowsUpdatedRuntimeException;
import sugita_r.exception.SQLRuntimeException;

public class SettinguserDao {


    public User getSettinguser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ? ";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);


            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String password = rs.getString("password");
                String user_name = rs.getString("user_name");
                String branch_office = rs.getString("branch_office");
                String department = rs.getString("department");
                String loginid = rs.getString("loginid");

                User settinguser = new User();
                settinguser.setId(id);
                settinguser.setPassword(password);
                settinguser.setName(user_name);
                settinguser.setBranch_office(branch_office);
                settinguser.setDepartment(department);
                settinguser.setLoginid(loginid);


                ret.add(settinguser);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  user_name = ?");
            sql.append(", branch_office = ?");
            sql.append(", department = ?");
            sql.append(", password = ?");
            sql.append(", loginid = ?");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getName());
            ps.setString(2, user.getBranch_office());
            ps.setString(3, user.getDepartment());
            ps.setString(4, user.getPassword());
            ps.setString(5, user.getLoginid());
            ps.setInt(6, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
}