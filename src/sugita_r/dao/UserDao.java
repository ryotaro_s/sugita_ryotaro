package sugita_r.dao;

import static sugita_r.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sugita_r.beans.BranchOffice;
import sugita_r.beans.Department;
import sugita_r.beans.User;
import sugita_r.exception.NoRowsUpdatedRuntimeException;
import sugita_r.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("password");
            sql.append(", user_name");
            sql.append(", branch_office");
            sql.append(", department");
            sql.append(", loginid");
            sql.append(", user");
            sql.append(") VALUES (");
            sql.append("?"); // department
            sql.append(", ?"); // password
            sql.append(", ?"); // user_name
            sql.append(", ?"); // branch_office
            sql.append(", ?"); // loginid
            sql.append(", ?"); // user
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getPassword());
            ps.setString(2, user.getName());
            ps.setString(3, user.getBranch_office());
            ps.setString(4, user.getDepartment());
            ps.setString(5, user.getLoginid());
            ps.setString(6, String.valueOf(1));

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public User getUser(Connection connection, String loginid,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE loginid = ? AND password = ? AND user = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, loginid);
            ps.setString(2, password);
            ps.setString(3, String.valueOf(1));

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String password = rs.getString("password");
                String user_name = rs.getString("user_name");
                String branch_office = rs.getString("branch_office");
                String department = rs.getString("department");
                String loginid = rs.getString("loginid");

                User user = new User();
                user.setId(id);
                user.setPassword(password);
                user.setName(user_name);
                user.setBranch_office(branch_office);
                user.setDepartment(department);
                user.setLoginid(loginid);


                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public User getUser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  password= ?");
            sql.append(", user_name= ?");
            sql.append(", branch_office= ?");
            sql.append(", department= ?");
            sql.append(", loginid= ?");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getPassword());
            ps.setString(2, user.getName());
            ps.setString(3, user.getBranch_office());
            ps.setString(4, user.getDepartment());
            ps.setString(5, user.getLoginid());
            ps.setInt(6, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public List<Department> getDepartment(Connection connection) {
        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM department";

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Department> ret = toDepartment(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    private List<Department> toDepartment(ResultSet rs)
            throws SQLException {

        List<Department> ret = new ArrayList<Department>();
        try {
            while (rs.next()) {
                int departmentid = rs.getInt("departmentid");
                String departmentname = rs.getString("departmentname");

                Department department = new Department();
                department.setDepartmentid(departmentid);
                department.setDepartmentname(departmentname);

                ret.add(department);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public List<BranchOffice> getBranchOffice(Connection connection) {
        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM branch_offices";

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<BranchOffice> ret = toBranchOffice(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    private List<BranchOffice> toBranchOffice(ResultSet rs)
            throws SQLException {

        List<BranchOffice> ret = new ArrayList<BranchOffice>();
        try {
            while (rs.next()) {
                int branchofficeid = rs.getInt("branch_officeid");
                String branchofficename = rs.getString("branch_officename");

                BranchOffice branchoffice = new BranchOffice();
                branchoffice.setBranch_officeid(branchofficeid);
                branchoffice.setBranch_officename(branchofficename);

                ret.add(branchoffice);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}