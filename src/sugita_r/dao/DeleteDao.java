package sugita_r.dao;

import static sugita_r.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import sugita_r.exception.SQLRuntimeException;

public class DeleteDao{

    public void delete(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "DELETE  FROM posts WHERE id=? ";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void commentdelete(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "DELETE  FROM comment WHERE id=? ";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}