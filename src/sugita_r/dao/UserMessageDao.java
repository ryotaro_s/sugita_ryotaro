package sugita_r.dao;

import static sugita_r.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import sugita_r.beans.UserMessage;
import sugita_r.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT*from posts ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                String category = rs.getString("category");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                Timestamp createdDate = rs.getTimestamp("created_date");
                String registeredPerson = rs.getString("registered_person");

                UserMessage message = new UserMessage();
                message.setSubject(subject);
                message.setText(text);
                message.setCategory(category);
                message.setId(id);
                message.setUserId(userId);
                message.setCreated_date(createdDate);
                message.setRegistered_person(registeredPerson);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}