package sugita_r.dao;

import static sugita_r.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import sugita_r.beans.UserMessage;
import sugita_r.exception.SQLRuntimeException;

public class SerchDao{

	public List<UserMessage> getDateserch(Connection connection, String d1,String d2,String categoryserches) {

        PreparedStatement ps = null;
        try {
        	String sql = "SELECT * FROM posts WHERE created_date >= ? AND created_date < ? "
        			+ "AND category LIKE CONCAT('%',?,'%') ORDER BY created_date DESC ";

            ps = connection.prepareStatement(sql);
            ps.setString(1,d1);
            ps.setString(2,d2);
            ps.setString(3,categoryserches);

            ResultSet rs = ps.executeQuery();
            List<UserMessage> userList = toDateserch(rs);
            return userList;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    private List<UserMessage> toDateserch(ResultSet rs) throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                String category = rs.getString("category");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                Timestamp createdDate = rs.getTimestamp("created_date");
                String registeredPerson = rs.getString("registered_person");

                UserMessage dateserch = new UserMessage();
                dateserch.setSubject(subject);
                dateserch.setText(text);
                dateserch.setCategory(category);
                dateserch.setId(id);
                dateserch.setUserId(userId);
                dateserch.setCreated_date(createdDate);
                dateserch.setRegistered_person(registeredPerson);

                ret.add(dateserch);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}