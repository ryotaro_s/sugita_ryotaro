package sugita_r.dao;

import static sugita_r.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sugita_r.beans.User;
import sugita_r.exception.SQLRuntimeException;

public class UsersDao {

    public List<User> getUsers(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT* ");
            sql.append("FROM (users ");
            sql.append("INNER JOIN branch_offices ON users.branch_office=branch_officeid) ");
            sql.append("INNER JOIN department ON users.department=departmentid ");
            sql.append("ORDER BY id DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            return userList;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs)
            throws SQLException {

        List<User> userList = new ArrayList<User>();
        try {
            while (rs.next()) {
                String password = rs.getString("password");
                String user_name = rs.getString("user_name");
                String branch_office = rs.getString("branch_office");
                String department = rs.getString("department");
                String loginid = rs.getString("loginid");
                String branch_officename = rs.getString("branch_officename");
                String departmentname = rs.getString("departmentname");
                int id = rs.getInt("id");
                String user = rs.getString("user");

                User users = new User();
                users.setPassword(password);
                users.setName(user_name);
                users.setBranch_office(branch_office);
                users.setDepartment(department);
                users.setLoginid(loginid);
                users.setBranch_officename(branch_officename);
                users.setDepartmentname(departmentname);
                users.setId(id);
                users.setUser(user);


                userList.add(users);
            }
            return userList;
        } finally {
            close(rs);
        }
    }
    public void accountmanagemnt(Connection connection, String id,String managementid) {

        PreparedStatement ps = null;
        try {
            String sql = "UPDATE users SET user=? WHERE id=? ";

            ps = connection.prepareStatement(sql);
            ps.setString(1, managementid);
            ps.setString(2, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}