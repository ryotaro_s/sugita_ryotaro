package sugita_r.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sugita_r.beans.User;
import sugita_r.service.UserService;

@WebServlet(urlPatterns = { "/usermanagement" })
public class UsermanagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("loginUser");
        int departmentid = Integer.parseInt(user.getDepartment());
        int branch_officeid = Integer.parseInt(user.getBranch_office());
        if(departmentid <= 1 && branch_officeid <= 1){
	        List<User> users = new UserService().getUsers();

	        request.setAttribute("users", users);

	        request.getRequestDispatcher("/usermanagement.jsp").forward(request, response);
        }else{
        	List<String> messages = new ArrayList<String>();
        	messages.add("権限がありません");
        	session.setAttribute("errorMessages", messages);
        	response.sendRedirect("./");
        }
    }
}