package sugita_r.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sugita_r.service.UserService;

@WebServlet(urlPatterns = { "/account_management" })
public class AccountManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    		String id = request.getParameter("users.id");
	    	String managementid = request.getParameter("action");
	    	new UserService().accountmanagement(id,managementid);

        response.sendRedirect("usermanagement");
        }
    }