package sugita_r.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import sugita_r.beans.UserMessage;
import sugita_r.service.SerchService;

@WebServlet(urlPatterns = { "/dateserch" })
public class DateserchServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	String categoryserches = request.getParameter("categoryserch");
    	String date1 = request.getParameter("date1");
    	String date2 = request.getParameter("date2");
    	if(StringUtils.isNotBlank(date1) && StringUtils.isNotBlank(date2)){
    		String d1 = date1+" 00:00:00";
    		String d2 = date2+" 23:59:59";

    		List<UserMessage> dateserch = new SerchService().getDateserch(d1,d2,categoryserches);
            request.setAttribute("dateserch", dateserch);

    	}else if(StringUtils.isNotBlank(date1) && StringUtils.isBlank(date2)){
    		Date nd2 =new Date();
    		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    		String d2 =sdf1.format(nd2).toString();
    		String d1 = date1+" 00:00:00";

    		List<UserMessage> dateserch = new SerchService().getDateserch(d1,d2,categoryserches);
            request.setAttribute("dateserch", dateserch);

    	}else if(StringUtils.isBlank(date1) && StringUtils.isNotBlank(date2)){
    		String d1 ="1900/01/01 00:00:00";
    		String d2 = date2+" 23:59:59";

    		List<UserMessage> dateserch = new SerchService().getDateserch(d1,d2,categoryserches);
            request.setAttribute("dateserch", dateserch);

    	}else if(StringUtils.isBlank(date1) && StringUtils.isBlank(date2)){
    		String d1 ="1900/01/01 00:00:00";
    		String d2 ="2100/12/31 23:59:59";

    		List<UserMessage> dateserch = new SerchService().getDateserch(d1,d2,categoryserches);
            request.setAttribute("dateserch", dateserch);
    	}
        request.getRequestDispatcher("top.jsp").forward(request, response);
    }
}