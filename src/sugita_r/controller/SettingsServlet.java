package sugita_r.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import sugita_r.beans.BranchOffice;
import sugita_r.beans.Department;
import sugita_r.beans.User;
import sugita_r.exception.NoRowsUpdatedRuntimeException;
import sugita_r.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("loginUser");
        int departmentid = Integer.parseInt(user.getDepartment());
        int branch_officeid = Integer.parseInt(user.getBranch_office());

        if(departmentid <= 1 && branch_officeid <= 1){
	    	String strID = request.getParameter("users.id");
	    	int id = Integer.parseInt(strID);
	        User settinguser = new UserService().getSettinguser(id);
	        List<Department> department = new UserService().getDpartments();
	        List<BranchOffice> branchoffice = new UserService().getBranchOffice();

	        request.setAttribute("settinguser", settinguser);
	        request.setAttribute("department", department);
	        request.setAttribute("branchoffice", branchoffice);

	        request.getRequestDispatcher("/settings.jsp").forward(request, response);
        }else{
        	List<String> messages = new ArrayList<String>();
        	messages.add("権限がありません");
        	session.setAttribute("errorMessages", messages);
        	request.getRequestDispatcher("./").forward(request, response);
        }
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User settinguser = getEditUser(request);

        if (isValid(request, messages) == true) {

            try {
                new UserService().update(settinguser);
            } catch (NoRowsUpdatedRuntimeException e) {
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("settinguser", settinguser);
                request.getRequestDispatcher("settings.jsp").forward(request, response);
                return;
            }



            response.sendRedirect("usermanagement");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("settinguser", settinguser);
            request.getRequestDispatcher("settings.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User settinguser = new User();
        settinguser.setId(Integer.parseInt(request.getParameter("id")));
        settinguser.setName(request.getParameter("user_name"));
        settinguser.setPassword(request.getParameter("password"));
        settinguser.setBranch_office(request.getParameter("branch_office"));
        settinguser.setDepartment(request.getParameter("department"));
        settinguser.setLoginid(request.getParameter("loginid"));
        return settinguser;
    }


    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String loginid = request.getParameter("loginid");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("user_name");
        Pattern p = Pattern.compile("[^0-9a-zA-Z]");
        Pattern p2 = Pattern.compile("[^0-9a-zA-Z\\p{Punct}]");

        if (20 < loginid.length() || 6 > loginid.length()) {
            messages.add("ログインIDは6文字以上20文字以下にしてください");
        }
        if (p.matcher(loginid).find()) {
            messages.add("ログインIDは半角英数字にしてください");
        }
        if (20 < password.length() || 6 > loginid.length()) {
            messages.add("パスワードは6文字以上20文字以下にしてください");
        }
        if (p2.matcher(password).find()) {
            messages.add("パスワードは半角文字にしてください");
        }
        if (!(password.equals(password2))) {
            messages.add("パスワードが一致しません");
        }
        if (10 < name.length()) {
            messages.add("名称は10文字以下にしてください");
        }
        if (StringUtils.isEmpty(name) == true) {
            messages.add("名称を入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}