package sugita_r.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sugita_r.service.DeleteService;

@WebServlet(urlPatterns = { "/delete" })
public class DeleteServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	if(request.getParameter("message.id") != null){
	    	String strID = request.getParameter("message.id");
	    	int id = Integer.parseInt(strID);
	    	new DeleteService().delete(id);
    	}else if(request.getParameter("comment.id") != null){
	    	String comID = request.getParameter("comment.id");
	    	int id = Integer.parseInt(comID);
	    	new DeleteService().commentdelete(id);
    	}

        response.sendRedirect("./");
    }
}