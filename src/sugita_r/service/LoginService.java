package sugita_r.service;

import static sugita_r.utils.CloseableUtil.*;
import static sugita_r.utils.DBUtil.*;

import java.sql.Connection;

import sugita_r.beans.User;
import sugita_r.dao.UserDao;
import sugita_r.utils.CipherUtil;

public class LoginService {

    public User login(String loginid, String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            User user = userDao.getUser(connection, loginid, encPassword);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}