package sugita_r.service;

import static sugita_r.utils.CloseableUtil.*;
import static sugita_r.utils.DBUtil.*;

import java.sql.Connection;

import sugita_r.dao.DeleteDao;

public class DeleteService {


		public void delete(int id) {

		    Connection connection = null;
		    try {
		        connection = getConnection();

		        DeleteDao deleteDao = new DeleteDao();
		        deleteDao.delete(connection, id);

		        commit(connection);

		    } catch (RuntimeException e) {
		        rollback(connection);
		        throw e;
		    } catch (Error e) {
		        rollback(connection);
		        throw e;
		    } finally {
		        close(connection);
		    }
		}
		public void commentdelete(int id) {

		    Connection connection = null;
		    try {
		        connection = getConnection();

		        DeleteDao deleteDao = new DeleteDao();
		        deleteDao.commentdelete(connection, id);

		        commit(connection);

		    } catch (RuntimeException e) {
		        rollback(connection);
		        throw e;
		    } catch (Error e) {
		        rollback(connection);
		        throw e;
		    } finally {
		        close(connection);
		    }
		}
}