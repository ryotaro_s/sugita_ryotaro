package sugita_r.service;

import static sugita_r.utils.CloseableUtil.*;
import static sugita_r.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import sugita_r.beans.BranchOffice;
import sugita_r.beans.Department;
import sugita_r.beans.User;
import sugita_r.dao.SettinguserDao;
import sugita_r.dao.UserDao;
import sugita_r.dao.UsersDao;
import sugita_r.utils.CipherUtil;

public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public User getUser(int userId) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            User user = userDao.getUser(connection, userId);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    private static final int LIMIT_NUM = 1000;
	public List<User> getUsers() {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            UsersDao usersDao = new UsersDao();
	            List<User> ret = usersDao.getUsers(connection, LIMIT_NUM);

	            commit(connection);

	            return ret;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }
		public User getSettinguser(int id) {

		    Connection connection = null;
		    try {
		        connection = getConnection();

		        SettinguserDao settinguserDao = new SettinguserDao();
		        User settinguser = settinguserDao.getSettinguser(connection, id);

		        commit(connection);

		        return settinguser;
		    } catch (RuntimeException e) {
		        rollback(connection);
		        throw e;
		    } catch (Error e) {
		        rollback(connection);
		        throw e;
		    } finally {
		        close(connection);
		    }
		}
	    public void update(User settinguser) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            String encPassword = CipherUtil.encrypt(settinguser.getPassword());
	            settinguser.setPassword(encPassword);

	            SettinguserDao settinguserDao = new SettinguserDao();
	            settinguserDao.update(connection, settinguser);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }
	    public void accountmanagement(String id,String managementid) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            UsersDao usersDao = new UsersDao();
	            usersDao.accountmanagemnt(connection, id, managementid);

	            commit(connection);

	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }
	    public List<Department> getDpartments() {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            UserDao userDao = new UserDao();
	            List<Department> ret = userDao.getDepartment(connection);

	            commit(connection);

	            return ret;

	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }
	    public List<BranchOffice> getBranchOffice() {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            UserDao userDao = new UserDao();
	            List<BranchOffice> ret = userDao.getBranchOffice(connection);

	            commit(connection);

	            return ret;

	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }
}