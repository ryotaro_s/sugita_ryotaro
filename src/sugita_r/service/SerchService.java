package sugita_r.service;

import static sugita_r.utils.CloseableUtil.*;
import static sugita_r.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import sugita_r.beans.UserMessage;
import sugita_r.dao.SerchDao;

public class SerchService {
	public List<UserMessage> getDateserch(String d1,String d2,String categoryserches) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        SerchDao serchDao = new SerchDao();
	        List<UserMessage> dateserch = serchDao.getDateserch(connection, d1,d2,categoryserches);

	        commit(connection);

	        return dateserch;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
}