package sugita_r.beans;

import java.io.Serializable;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String user_name;
    private String password;
    private String branch_office;
    private String department;
    private String user;
    private String loginid;
    private String branch_officename;
    private String departmentname;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return user_name;
	}
	public void setName(String name) {
		this.user_name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getBranch_office() {
		return branch_office;
	}
	public void setBranch_office(String branch_office) {
		this.branch_office = branch_office;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getDepartmentname() {
		return departmentname;
	}
	public void setDepartmentname(String departmentname) {
		this.departmentname = departmentname;
	}
	public String getBranch_officename() {
		return branch_officename;
	}
	public void setBranch_officename(String branch_officename) {
		this.branch_officename = branch_officename;
	}
}
