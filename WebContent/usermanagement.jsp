<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<a href="signup">ユーザー新規登録</a>
		<table border="1">
			<tr>
				<th>名前</th>
				<th>部署・役職</th>
				<th>支店</th>
				<th>ユーザー編集</th>
				<th>アカウント復活・停止</th>
			</tr>
			<c:forEach items="${users}" var="users">
				<div class="profile">
					<tr>
						<form action="settings" method="get">
							<td><input name="users.id" value="${users.id}" id="id"
								type="hidden" /> <span class="name"><c:out
										value="${users.name}" /></span></td>
							<td><span class="department"><c:out
										value="${users.departmentname}" /></span></td>
							<td><span class="branch_office"><c:out
										value="${users.branch_officename}" /></span></td>
							<td><input type="submit" value="編集" />
						</form>
						</td>
						<form action="account_management" method="get">
							<td><input name="users.id" value="${users.id}" id="id"
								type="hidden" /> <input name="users.name" value="${users.name}"
								id="id" type="hidden" /> <c:choose>
									<c:when test="${users.user==0}">
										<button type='submit' name='action' value='1'
											onClick="return confirm('このユーザーを復活させますか？')">復活</button>
									</c:when>
									<c:when test="${users.user==1}">
										<button type='submit' name='action' value='0'
											onClick="return confirm('このユーザーを停止しますか？')">停止</button></td>
							</c:when>
							</c:choose>
						</form>
					</tr>
				</div>
			</c:forEach>
		</table>
		<br> <br /> <a href="./">戻る</a>
	</div>
</body>
</html>