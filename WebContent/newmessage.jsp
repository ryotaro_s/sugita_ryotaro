<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="newMessage" method="post">
			●件名（30文字まで）<br />
			<textarea name="subject" cols="30" rows="1" class="tweet-box"></textarea>
			<br /> ●本文（1000文字まで）<br />
			<textarea name="text" cols="65" rows="10" class="tweet-box"></textarea>
			<br /> ●カテゴリー（10文字まで）<br />
			<textarea name="category" cols="30" rows="1" class="tweet-box"></textarea>
			<br /> <br /> <input type="submit" value="投稿"><br /> <a
				href="./">戻る</a>
		</form>
	</div>