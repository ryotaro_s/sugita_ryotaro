<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="settings" method="post">
			<br /> <input name="id" value="${settinguser.id}" id="id"
				type="hidden" /> <label for="name">名前</label> <input
				name="user_name" value="${settinguser.name}" id="name" /> <br /> <label
				for="loginid">ログインID</label> <input name="loginid"
				value="${settinguser.loginid}" id="loginid" /> <br /> 支店<select
				name="branch_office">
				<c:forEach items="${branchoffice}" var="branchoffice">
					<option value="${branchoffice.branch_officeid}"
						<c:if test="${settinguser.branch_office == branchoffice.branch_officeid}">selected</c:if>>${branchoffice.branch_officename}</option>
				</c:forEach>
			</select> <br /> 部署・役職<select name="department">
				<c:forEach items="${department}" var="department">
					<option value="${department.departmentid}"
						<c:if test="${settinguser.department == department.departmentid}">selected</c:if>>${department.departmentname}</option>
				</c:forEach>
			</select> <br /> <label for="password">パスワード</label> <input name="password"
				type="password" id="password" /> <br /> <label for="password2">パスワード(確認用)</label>
			<input name="password2" type="password" id="password" /> <br /> <input
				type="submit" value="登録" /> <br /> <a href="usermanagement">戻る</a>
		</form>
		<div class="copyright">Copyright(c)sugita ryotaro</div>
	</div>
</body>
</html>