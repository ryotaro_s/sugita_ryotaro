<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板システム</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="line-bc">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="newMessage">新規投稿画面</a>
				<a href="usermanagement">ユーザー管理</a>
				<a href="summary">サマリー</a>
				<a href="logout">ログアウト</a>
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
								<li><c:out value="${message}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if>
		</div>
		<div class="profile">
			<div class="name">
				<c:out value="${loginUser.name}" />
				さんでログインしています
			</div>
			<br>
		</div>

		<div class="search">
			<form action="dateserch" method="get">
				<label for="name">カテゴリー検索</label><br /> <input name="categoryserch"
					id="categoryserch" /><br> <label for="name">期日絞込み</label><br />
				<input name=date1 type="date"></input> ～<input name=date2
					type="date"></input> <input type="submit" value="検索" />
			</form>
			<br />
		</div>

		<c:forEach items="${messages}" var="message">
			<div class="messages">
				<form action="comment" method="post">
					<input name="message.id" value="${message.id}" id="id"
						type="hidden" /> <span class="subject">件名:<c:out
							value="${message.subject}" /></span><br> <span class="text">本文:<c:out
							value="${message.text}" /></span><br> <span class="category">カテゴリー:<c:out
							value="${message.category}" /></span><br> <span
						class="created_date">投稿日時:<c:out
							value="${message.created_date}" /></span><br> <span
						class="registered_person">投稿者:<c:out
							value="${message.registered_person}" /></span><br> <label
						for="name">コメント(500文字まで)</label><br /> <input name="comment"
						id="comment" /> <input type="submit" value="コメント" />
				</form>
				<c:if
					test="${loginUser.name.equals(message.registered_person) or loginUser.department==5}">
					<form action="delete" method="post">
						<input name="message.id" value="${message.id}" id="id"
							type="hidden" /> <input type="submit" value="削除" />
					</form>
				</c:if>
			</div>

			<c:forEach items="${comments}" var="comment">
				<c:if test="${message.id.equals(comment.messageid)}">
					<div class="comments">
						<span class="comment">コメント:<c:out
								value="${comment.comment}" /></span> <br> <span
							class="createdDate">コメント日時:<c:out
								value="${comment.createdDate}" /></span> <br> <span
							class="registered_person">投稿者:<c:out
								value="${comment.registered_person}" /></span> <br>
						<c:if test="${loginUser.name.equals(comment.registered_person)}">
							<form action="delete" method="post">
								<input name="comment.id" value="${comment.id}" id="id"
									type="hidden" /> <input type="submit" value="削除" />
							</form>
						</c:if>
						<br>
					</div>
				</c:if>
			</c:forEach>
			<br>
			<br>
		</c:forEach>

		<c:forEach items="${dateserch}" var="dateserch">
			<div class="messages">
				<form action="comment" method="get">
					<input name="message.id" value="${dateserch.id}" id="id"
						type="hidden" /> <span class="subject">件名:<c:out
							value="${dateserch.subject}" /></span><br> <span class="text">本文:<c:out
							value="${dateserch.text}" /></span><br> <span class="category">カテゴリー:<c:out
							value="${dateserch.category}" /></span><br> <span
						class="created_date">投稿日時:<c:out
							value="${dateserch.created_date}" /></span><br> <span
						class="registered_person">投稿者:<c:out
							value="${dateserch.registered_person}" /></span><br>
				</form>
				<br> <br>
			</div>
		</c:forEach>
		<a href="./"> TOPに戻る</a>
		</c:if>
		<div class="copyright">Copyright(c)SugitaRyotatro</div>
	</div>
</body>
</html>

